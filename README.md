# Bible In A Year



This has nothing to do with computers.

I made this because all the other Bible-in-a-Year regimens for the Catholic canon has a three-pronged system: Old Testament, Wisdom literature and poetry, and the New Testament.  My preference is once or at most twice daily.



The 1x Daily has a single Bible passage on a date.  It goes from Genesis to Revelation, starting from January 1st.

The 1x Daily has a single Bible passage on a date.  It goes from Genesis to Revelation, starting from the start of the liturgical year (September).

The 2x Daily has a Bible passage from the Old Testament (which could be read in the morning) and another from the New Testament (which could be read in the evening).  It goes from Genesis to Malachi and Matthew to Revelation, starting from January 1st.

The 2x Daily has a Bible passage from the Old Testament (which could be read in the morning) and another from the New Testament (which could be read in the evening).  It goes from Genesis to Malachi and Matthew to Revelation, starting from the start of the liturgical year (September).

The 2x Daily New Testament reading I've borrowed from the website of the Greek Orthodox Archdiocese of America.
https://www.goarch.org/-/read-the-orthodox-study-bible-in-a-year-


Anyone can edit, merge, and fork this with whatever corrections.
